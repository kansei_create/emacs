
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

;; package管理
(package-initialize)
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
        ("melpa" . "http://melpa.org/packages/")
        ("org" . "http://orgmode.org/elpa/")))
(add-to-list 'load-path "/usr/local/share/gtags")

(package-initialize)

;;atomのカラーにする
(add-to-list 'custom-theme-load-path "~/.emacs.d/atom-one-dark-theme/")
(load-theme 'atom-one-dark t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ahs-default-range (quote ahs-range-whole-buffer))
 '(package-selected-packages
   (quote
    (tern-auto-complete go-autocomplete go-mode company-tern dumb-jump swiper ivy js2-mode rainbow-delimiters company-c-headers auto-complete-c-headers irony yasnippet company-irony-c-headers atom-dark-theme company-irony python-mode neotree multi-term company auto-highlight-symbol auto-complete))))

;; line numberの表示
(require 'linum)
(global-linum-mode 1)

;; left command
(setq mac-command-modifier 'super)

;; tabサイズ
(setq default-tab-width 8)

(setq-default c-basic-offset 4     ;;基本インデント量4
              tab-width 4          ;;タブ幅4
              indent-tabs-mode nil)  ;;インデントをタブでするかスペースでするか

(setq dumb-jump-mode t)
(setq dumb-jump-default-project "")
(ivy-mode 1)

(setq c-tab-always-indent t);;tabでインデント
(defun my-c-c++-mode-init ()
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'defun-open 0))
  ;;(c-toggle-auto-newline 1))
  ;;(electric-layout-mode 1)
  ;;(setq-local electric-layout-rules '{} around))
(add-hook 'c-mode-hook 'my-c-c++-mode-init)
(add-hook 'c++-mode-hook 'my-c-c++-mode-init)

;;; *.~ とかのバックアップファイルを作らない
(setq make-backup-files nil)
;;; .#* とかのバックアップファイルを作らない
(setq auto-save-default nil)

;;メニューバーを消す
(menu-bar-mode -1)
					;
;; カーソルの点滅をやめる
(blink-cursor-mode 0)

;; カーソル行をハイライトする
(global-hl-line-mode t)

;;選択行をハイライト
(transient-mark-mode t)

;; 対応する括弧を光らせる
(show-paren-mode 1)
(setq show-paren-style 'expression)

;;; 起動時の画面はいらない
(setq inhibit-startup-message t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:background "MediumBlue"))))
 '(transient ((t (:background "red")))))

;;(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
;; '(ahs-default-range (quote ahs-range-whole-buffer))
;; '(package-selected-packages
;;   (quote
;;	(company-irony-c-headers company-irony ctags-update helm-gtags highlight-symbol python-mode js2-mode neotree auto-complete))))
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <down>")  'windmove-down)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <right>") 'windmove-right)

;; C++ style
(defun add-c++-mode-conf ()
  (c-set-style "stroustrup")  ;;スタイルはストラウストラップ
  (show-paren-mode t))        ;;カッコを強調表示する
;;(add-hook 'c++-mode-hook 'add-c++-mode-conf)

;; C style
(defun add-c-mode-common-conf ()
  (c-set-style "stroustrup")                  ;;スタイルはストラウストラップ
  (show-paren-mode t)                         ;;カッコを強調表示する
  )

;;treeを表示
(require 'neotree)
(global-set-key "\C-q" 'neotree-toggle)

;; 現在ポイントがある関数名をモードラインに表示
(which-function-mode 1)

;; スクロールは1行ごとに
(setq mouse-wheel-scroll-amount '(3 ((shift) . 5)))

;; スクロールの加速をやめる
(setq mouse-wheel-progressive-speed nil)

;; bufferの最後でカーソルを動かそうとしても音をならなくする
(defun next-line (arg)
  (interactive "p")
  (condition-case nil
      (line-move arg)
    (end-of-buffer)))

;; エラー音をならなくする
(setq ring-bell-function 'ignore)

;;括弧2個
(add-hook 'c-mode-common-hook
          (lambda ()
         ;;(define-key c-mode-base-map "\"" 'electric-pair)
         ;;(define-key c-mode-base-map "\'" 'electric-pair)
         (define-key c-mode-base-map "(" 'electric-pair)
         (define-key c-mode-base-map "[" 'electric-pair)
         (define-key c-mode-base-map "{" 'electric-pair)))

;;括弧2個
(add-hook 'python-mode-hook
          (lambda ()
            (define-key python-mode-map "\"" 'electric-pair)
            (define-key python-mode-map "\'" 'electric-pair)
            (define-key python-mode-map "(" 'electric-pair)
            (define-key python-mode-map "[" 'electric-pair)
            (define-key python-mode-map "{" 'electric-pair)))

;;括弧2個
(add-hook 'go-mode-hook
          (lambda ()
            (define-key go-mode-map "\"" 'electric-pair)
            (define-key go-mode-map "\'" 'electric-pair)
            (define-key go-mode-map "(" 'electric-pair)
            (define-key go-mode-map "[" 'electric-pair)
            (define-key go-mode-map "{" 'electric-pair)))

(defun electric-pair ()
  "Insert character pair without sournding spaces"
  (interactive)
  (let (parens-require-spaces)
    (insert-pair)))

(setq tags-table-list '("~/.emacs.d/soccer_tag"))
;;定義ジャンプのショートカットキー
(define-key global-map "\C-d" 'grep-find)
(define-key global-map "\C-t" 'pop-tag-mark)
(define-key global-map "\C-f" 'find-file)
(define-key global-map "\C-u" 'goto-line)
(define-key global-map "\C-p" 'forward-word)
(define-key global-map "\C-o" 'backward-word)
(define-key global-map "\C-j" 'copy-region-as-kill)

(global-set-key (kbd "\C-z")   'advertised-undo)
(global-set-key (kbd "\C-s")   'swiper)
(global-set-key (kbd "\C-w")   'dumb-jump-go)

;;指定文字のハイライト
(require 'auto-highlight-symbol)
(add-hook 'c-mode-hook 'auto-highlight-symbol-mode)
(add-hook 'c++-mode-hook 'auto-highlight-symbol-mode)
(add-hook 'python-mode-hook 'auto-highlight-symbol-mode)
(add-hook 'go-mode-hook 'auto-highlight-symbol-mode)

;;company-modeを起動するファイルの指定
(global-company-mode 0 )
(add-hook 'c-mode-hook 'company-mode)
(add-hook 'c++-mode-hook 'company-mode)

;;c言語の補完の決め事
(add-hook 'c-mode-hook
	  (lambda ()
(require 'company)
(setq company-idle-delay 0) ; デフォルトは0.5
(setq company-minimum-prefix-length 2) ; デフォルトは4
(setq company-selection-wrap-around t))) ; 候補の一番下でさらに下に行こうとすると一番上に戻る

;;c++の補完の決め事
(add-hook 'c++-mode-hook
	  (lambda ()
(require 'company)
(setq company-idle-delay 0) ; デフォルトは0.5
(setq company-minimum-prefix-length 2) ; デフォルトは4
(setq company-selection-wrap-around t))) ; 候補の一番下でさらに下に行こうとすると一番上に戻る

(setq irony-lang-compile-option-alist
      '((c++-mode . ("c++" "-std=c++11" "-lstdc++" "-lm"))
        (c-mode . ("c"))
        (objc-mode . '("objective-c"))))
(defun irony--lang-compile-option ()
  (irony--awhen (cdr-safe (assq major-mode irony-lang-compile-option-alist))
    (append '("-x") it)))

;;pythonの補完
(require 'python)
;;;;; PYTHONPATH上のソースコードがauto-completeの補完対象になる ;;;;;
(setenv "PYTHONPATH" "/Users/kansei/.pyenv/versions/3.7.3/lib/python3.7/site-packages")
(require 'jedi)
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

;;go言語の設定
;; Goのパスを通す
(add-to-list 'exec-path (expand-file-name "/usr/local/Cellar/go/1.12.7/bin/"))
;; go get で入れたツールのパスを通す
(add-to-list 'exec-path (expand-file-name "/Users/kansei/go/bin/"))
(add-hook 'go-mode-hook
          (lambda()
(require 'go-autocomplete)
(require 'auto-complete-config)
(ac-config-default)))

